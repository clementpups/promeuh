import React, { useState } from 'react';
import { IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonIcon, IonButton, IonContent, IonModal } from '@ionic/react';
import { logoFacebook, logoTwitter, logoYoutube, logoInstagram, logoAppleAppstore, logoGooglePlaystore } from 'ionicons/icons';
import './ExploreContainer.css';
import Map from '../pages/map/map';

export const ExploreContainer: React.FC = () => {
  const [showModal, setShowModal] = useState(false);

return (
    <IonContent color="secondary">
      <IonGrid>
        <IonRow>  
          <IonCol size="12">
          <IonCard>
              <IonCardHeader>
                <IonCardTitle>Qui somme nous ?</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
              L'initiative Promeuh est un groupe de personnes voulant étendre le concept de manger mieux et manger local.
La mission de Promeuh, en plus de vous apporter des produits locaux issus de l'agriculture biologique tout en limitant l'empreinte carbone du transport, et d'accompagner les agriculteurs et les consommateurs vers des comportements plus respectueux de l'environnement.
Promeuh cherche également à rapprocher les communautés et à promeuhvoir les collaborations en collectivité.

Faites un pas vers un futur plus bio !
--L'équipe Promeuh
              </IonCardContent>
            </IonCard>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size="6">
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>Producteur</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonButton fill="outline" slot="end">Nous rejoindre</IonButton>
              </IonCardContent>
            </IonCard>
          </IonCol>
          <IonCol size="6">
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>Trouvez près de chez moi</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonButton fill="outline" slot="start"><IonIcon icon={logoAppleAppstore} size="large"></IonIcon> Lien Apple Store</IonButton>
                <IonButton fill="outline" slot="end"><IonIcon icon={logoGooglePlaystore} size="large"></IonIcon> Lien Google Play</IonButton>
                
                  <IonModal isOpen={showModal}>
                    <Map></Map>
                    <IonButton fill="outline" slot="start" onClick={() => setShowModal(false)}>
                        Fermer la carte
                    </IonButton>
                  </IonModal>
                  <IonButton fill="outline" slot="end" onClick={() => setShowModal(true)}>
                        Trouver pres de chez moi
                  </IonButton>             
              </IonCardContent>
            </IonCard>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size="12">
          <IonCard>
              <IonCardHeader>
                <IonCardTitle>Des questions ?</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonRow>
                  <IonCol size="2">
                    <IonIcon icon={logoFacebook} size="large"></IonIcon>
                  </IonCol>
                  <IonCol size="2">
                    <IonIcon icon={logoTwitter} size="large"></IonIcon>
                  </IonCol>
                  <IonCol size="2">
                    <IonIcon icon={logoYoutube} size="large"></IonIcon>
                  </IonCol>
                  <IonCol size="2">
                    <IonIcon icon={logoInstagram} size="large"></IonIcon>
                  </IonCol>
                  <IonCol size="3">
                    <IonButton fill="outline">Contacter nous !</IonButton>
                  </IonCol>
                </IonRow>
                
              </IonCardContent>
            </IonCard>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  );
}
export default ExploreContainer;
