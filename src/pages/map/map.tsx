import React, {  useState } from 'react';
import '../Home.scss';
import {IonHeader, IonContent, IonToolbar, IonTitle, IonItem, IonLabel, IonText, IonLoading, IonToast, IonIcon} from '@ionic/react';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import ReactMapGL, { Marker} from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { pin } from 'ionicons/icons';
const TOKEN = "pk.eyJ1IjoiY2xlbWVudHB1cHMiLCJhIjoiY2tnajR5eDZ2MGQ2ajJwbjR4NzN3OHV5NyJ9.z9BXTByq98vMBI8pwqzLsg"
interface LocationError {
  showError: boolean;
  message?: string;
}

interface Viewport {
    /**
     * a double representing the position's latitude in decimal degrees.
     */
    latitude: number;
    /**
     * A double representing the position's longitude in decimal degrees.
     */
    longitude: number;
    
    zoom: number;
  
}



const GeolocationButton: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<LocationError>({ showError: false });
  const [position, setPosition] = useState<Viewport>();
  
  const getLocation = async () => {
      setLoading(true);

      try {
          var buffer: Geoposition;
          buffer = await Geolocation.getCurrentPosition();
          const position = {
            latitude:  buffer.coords.latitude,
            longitude: buffer.coords.longitude,
            zoom : 10
          }

          setPosition(position);
          setLoading(false);
          setError({ showError: false });
      } catch (e) {
          setError({ showError: true, message: e.message });
          setLoading(false);
      }
  }


  return (
      <>
          <IonLoading
              isOpen={loading}
              onDidDismiss={() => setLoading(false)}
              message={'Getting Location...'}
          />
          <IonToast
              isOpen={error.showError}
              onDidDismiss={() => setError({ message: "", showError: false })}
              message={error.message}
              duration={3000}
          />
          
            <ReactMapGL
            {...position}
            height="75%"
            width="100%"
            mapboxApiAccessToken={TOKEN}
            onLoad={(e:any)=>{
              getLocation();  
            }}
            onViewportChange={(e:any)=>{
              setPosition(e);
            }}>
            <Marker latitude={(position?.latitude || 0 )} longitude={(position?.longitude || 0 )}>
                <IonIcon icon={pin} size="medium" color='Dark'/>
              </Marker>  
            
            </ReactMapGL>
      </>
  );
};

class Map extends React.Component {
  render() {
      return <>
        <IonHeader>
          
          <IonToolbar color="primary">
            <IonTitle>Carte</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <IonItem>
            <IonLabel>
              <IonText color="bud-green"> 
                  <h3>Map</h3>
              </IonText>
            </IonLabel>
          </IonItem>          
            <GeolocationButton />          
        </IonContent>
      </>
   };
   
}
export default Map;